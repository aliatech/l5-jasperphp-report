<?php

namespace AliaTech\Report;

class JasperBridge
{

  /**
   * JRXML path
   * @var string
   */
  public $jrxml_path;

  /**
   * Output path without extension
   * @var string
   */
  public $output_path;

  /**
   * Output formats
   * @var [string]
   */
  public $formats;

  /**
   * Map of attributes to fill the report
   * @var [string key => mixed value] Attributes
   */
  public $attributes;

  /*
   * Map of mixed JasperStarter arguments
   * @var [string key => string value]
   *  Example = [ // to fill report with XML datasource
   *    '-t' => 'xml',
   *    '--data-file' => '/path/to/file.xml',
   *    '--xml-xpath' => '/root'
   *  ]
   */
  public $jstarter_args;

  /**
   * DB connection attributes
   * @var [string key => string value] Access configuration to a database
   *  Example = [
   *    'driver' => 'postgres',
   *    'username' => 'vagrant',
   *    'host' => 'localhost',
   *    'database' => 'samples',
   *    'port' => '5433',
   *  ]
   */
  public $db_connection;

  /**
   * Whether to process the report in background
   * @var boolean
   */
  public $background;

  /**
   * Whether to redirect the execution output
   * @var boolean
   */
  public $redirect_output;

  /**
   * Class constructor
   */
  public function __construct()
  {
    $this->formats = ['pdf'];
    $this->attributes = [];
    $this->jstarter_args = [];
    $this->db_connection = [];
    $this->background = false;
    $this->redirect_output = true;
  }

  /**
   * Extend the map of attributes
   * @param Attributes $attributes
   */
  public function extendAttributes($attributes)
  {
    $this->attributes = array_merge($this->attributes, $attributes);
  }

  /**
   * Extend the map of JasperStarter arguments
   * @param [string key => string value] $args
   */
  public function extendJstarterArgs($args)
  {
    $this->jstarter_args = array_merge($this->jstarter_args, $args);
  }

}
