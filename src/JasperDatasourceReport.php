<?php

namespace AliaTech\Report;

use JasperPHP\JasperPHP;


abstract class JasperDatasourceReport extends JasperReport
{

  /**
   * Datasource type
   * @var string
   */
  public $datasource_type;

  /**
   * Datasource dataset parameters
   * @var mixed
   */
  protected $dataset;

  /**
   * Default XML X-Path
   * @var string
   */
  protected $xml_xpath = '';

  /**
   * @see parent::_initBridge
   */
  protected function _initBridge()
  {
    parent::_initBridge();
    if (empty($this->dataset)) return;
    $this->bridge->extendJstarterArgs([
      '-t' => $this->datasource_type,
      '--data-file' => $this->_initDataFilePath()
    ]);
  }

  /**
   * Init Jasper Bridge datasource file path
   */
  protected function _initDataFilePath()
  {
    $path = config('report.datafile_path') .'/'. $this->datasource_type .'/'. pathinfo($this->bridge->output_path, PATHINFO_FILENAME) .'.'. $this->datasource_type;
    $dir = pathinfo($path, PATHINFO_DIRNAME);
    if (!\File::exists($dir)) \File::makeDirectory($dir, 0755, true);
    return $path;
  }

  /**
   * Write data file
   * @param string $content
   */
  protected function _writeDataFile($content)
  {
    file_put_contents($this->bridge->jstarter_args['--data-file'], $content);
  }

  /**
   * Generate data file from dataset parameters
   */
  public function generateDataFile()
  {
    if (empty($this->dataset)) return;
    switch($this->datasource_type){
      case 'xml': return $this->_generateXmlDataFile();
      // add more generators
    }
  }

  /**
   * Generate XML dat file from dataset parameters
   */
  protected function _generateXmlDataFile()
  {
    // creating object of SimpleXMLElement
    $xml_data = new \SimpleXMLElement('<?xml version="1.0"?><data></data>');
    // function call to convert array to xml
    $this->_array2xml($this->dataset, $xml_data);
    $xml = $xml_data->asXML();
    $this->_writeDataFile($xml);
    $this->bridge->extendJstarterArgs([
      '--xml-xpath' => '/data'. $this->xml_xpath
    ]);
  }

  /**
   * Parse array to XML
   * @param array $data
   * @param SimpleXMLElement $xml_data
   * @param string $parent_key
   */
  protected function _array2xml($data, &$xml_data, $parent_key = null)
  {
    foreach ($data as $key => $value){
      if (is_array($value)){
        if (is_numeric($key)){
          $key = !is_null($parent_key) ? $parent_key : 'item'. $key; //dealing with <0/>..<n/> issues
        }
        $subnode = array_key_exists(0, $value) ? $xml_data : $xml_data->addChild($key);
        $this->_array2xml($value, $subnode, $key);
      }else{
        if (is_numeric($key)) $key = $parent_key;
        $xml_data->addChild("$key", htmlspecialchars("$value"));
      }
    }
  }

  /**
   * Parse array of tags to xml, setting first as root and each sibling deeper
   * @param [string] $tokens
   * @return string
   */
  protected function _tokens2xml($tokens)
  {
    $token = array_unshift($tokens);
    if (empty($token)) return '';
    return "<$token>". $this->_tokens2xml($tokens) ."</$token>";
  }

  /**
   * @see parent::generate
   */
  public function generate()
  {
    $this->generateDataFile();
    parent::generate();
  }

}
