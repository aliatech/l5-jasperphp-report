<?php

namespace AliaTech\Report;

use Illuminate\Support\ServiceProvider;

class ReportServiceProvider extends ServiceProvider
{

  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
    // include config
    $this->publishes([
        __DIR__ .'/config/report.php' => config_path('report.php'),
    ]);
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
    // include routes
    if (!$this->app->routesAreCached())
      require __DIR__ .'/app/routes.php';
    // include controllers
    $this->app->make('AliaTech\Report\Controllers\ReportController');
  }

}
