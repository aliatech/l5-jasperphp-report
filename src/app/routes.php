<?php

Route::get('/report/{type}/generate', ['as' => 'aliatech.report.generate', 'uses' => 'AliaTech\Report\Controllers\ReportController@generateAndShow']);

if (app()->isLocal()){

  Route::get('/aliatech/report', function(){
    return 'It works!';
  });

  Route::get('/test/report', 'AliaTech\Report\Controllers\ReportController@test');

  // Route to test example.input with route parameters
  Route::get('/report/{type}/generate/{a}/{b}', ['as' => 'aliatech.report.generate', 'uses' => 'AliaTech\Report\Controllers\ReportController@generateAndShow']);

}
