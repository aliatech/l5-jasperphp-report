<?php

return [

  /*
	 |--------------------------------------------------------------------------
	 | Report paths
	 |--------------------------------------------------------------------------
	 */
   /**
    * Path to default directory to save reports
    */
  'output_path' => storage_path('app/reports'),
   /**
    * Path to default directory to save datasource files.
    * Final path will be calculated this way: {datafile_path}/{type}/{output_filename}.{type}
    */
  'datafile_path' => storage_path('app/reports/data'),

	/*
	 |--------------------------------------------------------------------------
	 | Report classes
	 |--------------------------------------------------------------------------
	 */
   /**
    * Map of existing report classes indexed by a key
    */
  'classes' => [

    'example' => [

      // Example accepting parameters
      'helloworld'  => AliaTech\Report\Examples\HelloWorldReport::class,

      // Example accepting parameters and generating XML datasource
      'addressbook' => AliaTech\Report\Examples\AddressBookReport::class,

      // Example accepting parameters and checking that JasperDatasourceReport works even though dataset is empty
      'empty-datasource' => AliaTech\Report\Examples\EmptyDatasourceReport::class,

      // Example accepting parameters and input data from request
      'input' => AliaTech\Report\Examples\InputReport::class

    ]

  ]

];

?>
