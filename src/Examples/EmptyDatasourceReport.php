<?php

namespace AliaTech\Report\Examples;

use AliaTech\Report\JasperDatasourceReport;

/**
 * Example to test that JasperDatasourceReport works even though dataset is empty
 */
class EmptyDatasourceReport extends JasperDatasourceReport
{

  /**
   * @see parent::$datasource_type
   */
  public $datasource_type = 'xml';

  /**
   * @see parent::_initJrxmlPath
   */
  protected function _initJrxmlPath()
  {
    return base_path('vendor/aliatech/jasperphp/examples/hello_world.jrxml');
  }

  /**
   * @see parent::_initAttributes
   */
  protected function _initAttributes()
  {
    return ['php_version' => phpversion()];
  }

}
