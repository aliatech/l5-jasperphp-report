<?php

namespace AliaTech\Report\Examples;

use AliaTech\Report\JasperReport;


class HelloWorldReport extends JasperReport
{

  /**
   * @see parent::_initJrxmlPath
   */
  protected function _initJrxmlPath()
  {
    return base_path('vendor/aliatech/jasperphp/examples/hello_world.jrxml');
  }

  /**
   * @see parent::_initAttributes
   */
  protected function _initAttributes()
  {
    return ['php_version' => phpversion()];
  }

}
